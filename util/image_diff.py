# Copyright (c) 2020 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from PIL import Image, ImageChops
import io

def image_diff_png(ref_png, actual_png):
    """Creates PNG diff image data from reference and actual PNG data

    Args:
        ref_png: PNG data for the reference image
        actual_png: PNG data for the actual image

    Returns:
        Bytes representing the diff image as PNG
    """
    ref_image = Image.open(io.BytesIO(ref_png)).convert("RGBA")
    actual_image = Image.open(io.BytesIO(actual_png)).convert("RGBA")

    diff_image = ImageChops.difference(ref_image, actual_image)

    # Replace all differences with opaque red pixels
    pixel_count = 0
    for y in range(diff_image.size[1]):
        for x in range(diff_image.size[0]):
            if diff_image.getpixel((x, y)) != (0, 0, 0, 0):
                diff_image.putpixel((x, y), (255, 0, 0, 255))
                pixel_count += 1

    # Get a grayscale background image
    out_image = ref_image.convert("LA").convert("RGBA")
    # And overlay the red diff pixels
    out_image.paste(diff_image, (0,0), diff_image)

    out_io = io.BytesIO()
    out_image.save(out_io, format="png")
    out_io.seek(0)
    return out_io.read(), pixel_count
