import subprocess

try:
    GIT_COMMIT_ID = subprocess.run(["git", "rev-parse", "--short", "HEAD"], capture_output=True, encoding='utf-8').stdout.strip()
except:
    GIT_COMMIT_ID = "error"

def git_commit_id(request):
    return {'GIT_COMMIT_ID': GIT_COMMIT_ID}
